Pod::Spec.new do |s|
    s.name                = "Mati-iOS"
    s.version             = "1.0.2"
    s.summary             = "Use Mati for easy pay"
    s.description         = "Use Mati for easy and fast pay."
    s.homepage            = "https://bitbucket.org/mati_face/mati-ios-framework"
    s.license             = { type: 'MIT', file: 'LICENSE' }
    s.authors             = "Mati Face"
    s.platform            = :ios, "9.0"
    s.source              = { :git => "https://bitbucket.org/mati_face/mati-ios-framework.git",  :tag => "1.0.2"  }
    s.frameworks          = 'UIKit'
    s.source_files        = "MatiiOSSDK.framework/Headers/*.h"
    s.vendored_frameworks = "MatiiOSSDK.framework"
    s.public_header_files = "MatiiOSSDK.framework/Headers/*.h"
end


